package fr.cnam.foad.nfa035.badges.wallet.dao.impl;

import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.impl.ImageFileFrame;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.ImageStreamingDeserializer;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.ImageStreamingSerializer;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.impl.ImageDeserializerBase64StreamingImpl;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.impl.ImageSerializerBase64StreamingImpl;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;

public class MultiBadgeWalletDAOimplImpl extends SingleBadgeWalletDAOImpl implements BadgeWalletDAO{

    private File walletDatabase;
    private ImageFileFrame media;

    public MultiBadgeWalletDAOimplImpl(String s) {
        super();
    }

    /**
     * Permet d'ajouter le badge au Wallet
     *
     * @param image
     * @throws IOException
     */
    public void addBadge(File image) throws IOException {
        ImageStreamingSerializer serializer = new ImageSerializerBase64StreamingImpl();
        serializer.serialize(image, media);
    }

    /**
     * Permet de récupérer le badge du Wallet
     *
     * @param imageStream
     * @throws IOException
     */
    public void getBadge(OutputStream imageStream) throws IOException{
        ImageStreamingDeserializer deserializer = new ImageDeserializerBase64StreamingImpl(imageStream);
        deserializer.deserialize(media);
    }

    @Override
    public void addBadge() {

    }

    @Override
    public void getBadge() {

    }
}
