package fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media;

import java.io.BufferedReader;
import java.io.IOException;

/**
* Interface qui reprend le média
 */

public interface ResumableImageFrameMedia extends ImageFrameMedia {

    /**
    * Méthode qui lit ou non le média encodé  .
     */

    BufferedReader getEncodedImageReader(boolean resume) throws IOException;
}
