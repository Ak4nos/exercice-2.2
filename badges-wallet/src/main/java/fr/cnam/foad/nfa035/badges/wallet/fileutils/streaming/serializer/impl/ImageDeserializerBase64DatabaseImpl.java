package fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.impl;

import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.ImageFrameMedia;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.ResumableImageFrameMedia;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.DatabaseDeserializer;
import org.apache.commons.codec.binary.Base64InputStream;
import org.apache.commons.codec.binary.Base64OutputStream;

import java.io.*;
import java.nio.file.Files;

public class ImageDeserializerBase64DatabaseImpl implements DatabaseDeserializer<ResumableImageFrameMedia> {


    @Override
    public <T extends OutputStream> T getSourceOutputStream() {
        return null;
    }

    @Override
    public <T extends OutputStream> void setSourceOutputStream(T os) {

    }


    @Override
    public void deserialize(ResumableImageFrameMedia media) throws IOException {

        BufferedReader br = media.getEncodedImageReader(true);
        String[] data = br.readLine().split(";");
        try (OutputStream os = getSourceOutputStream()) {
            getDeserializingStream(data[2]).transferTo(os);
        }
    }

    // 1. Récupération de l'instance de lecture séquentielle du fichier de base csv

        // 2. Lecture de la ligne et parsage des différents champs contenus dans la ligne
        // 3. Désérialisation de l'image Base64 + écriture en clair dans le flux de restitution au format source


    @Override
    public InputStream getDeserializingStream(String data) throws IOException {
        return new Base64InputStream(new ByteArrayInputStream(data.getBytes()));
    }
}
